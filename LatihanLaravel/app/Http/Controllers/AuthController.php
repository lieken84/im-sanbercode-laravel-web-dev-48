<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('daftarin');
    }

    public function selesai(Request $request)
    {
        $NamaDepan = $request->input('FirstName');
        $NamaBelakang = $request->input('LastName');
    return view ('congrats', ['NamaDepan' => $NamaDepan, 'NamaBelakang' => $NamaBelakang]);
        }
    }