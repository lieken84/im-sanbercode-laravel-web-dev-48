@extends('layout.master')
@section('judul')
Buat Account Baru!
@endsection
@section('content')
   <h2>Sign Up Form</h2>
   <form action="/sukses" method="post">
      @csrf
      <label for="">First name:</label>
      <br><br>
      <input type="text" name="FirstName">
      <br><br>
      <label for="">Last name:</label>
      <br><br>
      <input type="text" name="LastName">
      <br><br>
      <label for="">Gender:</label>
      <br><br>
      <input type="radio" value="1" name="Gender">Male
      <br>
      <input type="radio" value="2" name="Gender">Female
      <br>
      <input type="radio" value="3" name="Gender">Other
      <br><br>
      <label for="">Nationality:</label>
      <br><br>
      <select name="Negara" id="">
         <option value="">Indonesian</option>
         <option value="">American</option>
         <option value="">Japanese</option>
      </select>
      <br><br>
      <label for="">Language Spoken:</label>
      <br><br>
      <input type="checkbox" name="language spoken">Bahasa Indonesia
      <br>
      <input type="checkbox" name="language spoken">English
      <br>
      <input type="checkbox" name="language spoken">Other
      <br><br>
      <label for="">Bio:</label>
      <br><br>
<textarea name="biodata" id="" cols="30" rows="10"></textarea>
<br>
   <input type="submit" value="Sign Up" name="" id="">
   </form>
@endsection