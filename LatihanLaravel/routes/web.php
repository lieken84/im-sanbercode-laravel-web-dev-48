<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'utama']);
Route::get('/daftarin', [AuthController::class,'daftar']);
Route::post('/sukses', [AuthController::class,'selesai']);
Route::get('/data-tables', function(){
	return view('data-tables');
});
Route::get('/table', function(){
	return view('table');
});

//CRUD Cast

//C=>Create Data Route yang mengarah ke halaman tambah cast
Route::get('/cast/create', [CastController::class,'create']);
//Route yang menyimpan data inputan masuk ke database & validasi
Route::post('/cast', [CastController::class, 'store']);

//R=>Route yang menampilkan semua data di database ke blade web browsernya
Route::get('/cast',[CastController::class,'index']);
//Route detail data berdasarkan id
Route::get('/cast/{id}',[CastController::class,'show']);

//U => Update Data
// route untuk mengarah ke halaman form update data berdasarkan id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//route untuk update data ke database berdasarkan id
Route::put('/cast/{id}',[CastController::class,'update']);

//D => delete data
// route mendelete data berdasarkan id
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
